package org.nanobot

class Channel {
    def topic
    def String name
    def ArrayList<String> users = [:]
    def ArrayList<String> ops = [:]
    def ArrayList<String> voices = [:]

    @Override
    String toString() {
        return name
    }
}
