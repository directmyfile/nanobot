NanoBot
=======

NanoBot is an IRC Framework written in Groovy. Written to be used in a Groovy Environment, it is fast and has support for many IRC features.

[![Build Status](https://minetweak.ci.cloudbees.com/job/NanoBot/badge/icon)](https://minetweak.ci.cloudbees.com/job/NanoBot/)

Building
--------

Use the [Gradle Wrapper](http://www.gradle.org/docs/current/userguide/gradle_wrapper.html) to build NanoBot.

Using
-----

Examples:
- [ForgeEssentials Bot](https://github.com/ForgeEssentials/cloaked-octo-robot)
- [NanoBot Test](https://github.com/DirectMyFile/NanoBot/blob/master/src/main/groovy/TestNanoBot.groovy)

Javadocs are coming soon.

Features
--------

- Private/Channel messaging
- Custom Closure-based Threaded Event Bus
- Kick/Ban/Kickban support
- Mode support
- Topic support
- Error handling
- Built-in Command Handler
- Channel Management
- Scriptable
